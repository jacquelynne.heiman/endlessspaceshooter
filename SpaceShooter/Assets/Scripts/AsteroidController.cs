﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class AsteroidController : MonoBehaviour
{
    private Rigidbody2D rb;
    private float AsteroidSpeed = .1f;

    public GameObject player;

    private Vector3 moveDirection;

    private GameManager gameManager;




    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        gameManager = GameObject.FindObjectOfType<GameManager>();
        rb = GetComponent<Rigidbody2D>();
        moveDirection = (player.transform.position - transform.position);
        

    }

    // Update is called once per frame
    void Update()
    {
        transform.position += moveDirection * AsteroidSpeed * Time.deltaTime;

        if (transform.position.x > 2.2f || transform.position.x < -2.2f || transform.position.y > 4f ||
            transform.position.y < -4f)
        {
            gameManager.AsteroidsInField.Remove(gameObject);
            Destroy(gameObject);
            gameManager.AsteroidsInGame--;

        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            gameManager.AsteroidsInField.Remove(gameObject);
            Destroy(gameObject);
            gameManager.AsteroidsInGame--;
            gameManager.playerHealth -= .2f;
        }

        if (other.gameObject.CompareTag("Enemy1"))
        {
            gameManager.AsteroidsInField.Remove(gameObject);
            gameManager.AsteroidsInGame--;
            gameManager.currentEnemiesInGame.Remove(other.gameObject);
            gameManager.EnemiesInGame--;
            Destroy(other.gameObject);
            Destroy(gameObject);

        }
    }
}
