﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class BulletMover : MonoBehaviour
{
    Rigidbody2D rb;
    float shotSpeed = 3f;
    float bulletDestroy = 2f;

    private GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.up * shotSpeed;
        Destroy(gameObject, bulletDestroy);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Asteroid"))
        {
            gameManager.AsteroidsInField.Remove(other.gameObject);
            Destroy(other.gameObject);
            Destroy(gameObject);
            gameManager.AsteroidsInGame--;
            gameManager.score++;
        }

        if (other.gameObject.CompareTag("Player"))
        {
            gameManager.playerHealth -= .2f;
            Destroy(gameObject);
        }

        if (other.gameObject.CompareTag("Enemy1"))
        {
            gameManager.EnemiesInGame--;
            gameManager.currentEnemiesInGame.Remove(other.gameObject);
            Destroy(other.gameObject);
            Destroy(gameObject);

            gameManager.score += 20;
        }

        if (other.gameObject.CompareTag("Bullet"))
        {
            Destroy(other.gameObject);
        }
    }

}
