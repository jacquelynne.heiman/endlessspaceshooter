﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.VersionControl;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public bool seesPlayer;
    private Transform playersTrans;

    public float rotationSpeed;
    public float speed;

    public float ShootTimer;
    public float startTime;

    public GameObject bulletPrefab;
    public Transform ShotSpawn;

    public GameManager gameManager;

    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    void Update()
    {
        transform.Translate(Vector3.right * speed * Time.deltaTime);

        if (seesPlayer)
        {
            LookAtPlayer(playersTrans);
        }

        if (transform.position.x > 2f || transform.position.x < -2f || transform.position.y > 3.5f ||
            transform.position.y < -3.5f)
        {
            gameManager.currentEnemiesInGame.Remove(gameObject);
            Destroy(gameObject);
            gameManager.EnemiesInGame--;

        }
    }
    

    void OnTriggerEnter2D (Collider2D other)
    {
        Debug.Log("Something entered me");
        if(other.gameObject.CompareTag("Player"))
        {
            seesPlayer = true;
            playersTrans = other.gameObject.transform;
            Debug.Log(seesPlayer);
        }
    }

    void OnTriggerExit2D (Collider2D other)
    {
        Debug.Log("Something exited me");

        if (other.gameObject.CompareTag("Player"))
        {
            seesPlayer = false;
            Debug.Log(seesPlayer);
        }
    }

    void LookAtPlayer(Transform playerTransform)
    {
        Vector3 vectorToTarget = playerTransform.position - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, q, 180f);

        Chase();

    }

    void Chase()
    {
        transform.Translate(Vector3.right * speed * Time.deltaTime);

        if (Vector2.Distance(transform.position, playersTrans.position) < 2.5f)
        {

            Debug.Log("CloseEnough");
            speed = 0;

            Shoot();
        }

        if (Vector2.Distance(transform.position, playersTrans.position) > 2.5f)
        {
            speed = 1;
            
        }
    }

    void Shoot()
    {

        if (Time.time > startTime + ShootTimer)
        {
            Instantiate(bulletPrefab, ShotSpawn.position, ShotSpawn.rotation);
            startTime = Time.time;
        }
    }
}
