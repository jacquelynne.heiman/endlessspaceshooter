﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Image HealthFill;
    public Image ChargeFill;

    public PlayerController player;

    public List<Transform> SpawnLocations;
    public List<GameObject> AsteroidPrefabs;

    public List<GameObject> AsteroidsInField; 

    public int AsteroidsInGame = 0;
    public int MaxInGame = 3;
    public int EnemiesInGame = 0;
    public int MaxEnemiesInGame = 1;

    public int score;
    public int hiscore;

    public Text currentScore;
    public Text HiScore;

    public float playerHealth = 1f;

    public Animator playerAnimator;

    public int playerLives;

    public Image[] lives;

    public List<Transform> EnemySpawnLocations;

    public GameObject enemyPrefab;

    public List<GameObject> currentEnemiesInGame;


    void Awake()
    {
        Debug.Log(PlayerPrefs.GetInt("hiscore"));
        HiScore.text = "High Score:" + PlayerPrefs.GetInt("hiscore", 0).ToString();

        hiscore = PlayerPrefs.GetInt("hiscore");
    }

    // Update is called once per frame
    void Update()
    {

        if (AsteroidsInGame < MaxInGame)
        {
            SpawnAsteroids();
        }

        if (EnemiesInGame < MaxEnemiesInGame)
        {
            SpawnEnemies();
        }

        if (score > hiscore)
        {
            hiscore = score;
            SaveHighScore();
        }

        if (playerHealth > 1)
        {
            playerHealth = 1;
        }

        if (playerHealth < 0)
        {
            playerHealth = 0;
        }

        playerAnimator.SetFloat("playerHealth", playerHealth);

        UpdateUI();
    }

    void SpawnAsteroids()
    {
        int spawnAt = Random.Range(0, SpawnLocations.Count);

        int asteroid = Random.Range(0, AsteroidPrefabs.Count);

        GameObject newAsteroid = Instantiate(AsteroidPrefabs[asteroid], SpawnLocations[spawnAt].position, SpawnLocations[spawnAt].rotation);
        AsteroidsInGame++;

        AsteroidsInField.Add(newAsteroid);
    }

    void SpawnEnemies()
    {
        int spawnAt = Random.Range(0, EnemySpawnLocations.Count);

        GameObject newEnemy = Instantiate(enemyPrefab, EnemySpawnLocations[spawnAt].position,
            EnemySpawnLocations[spawnAt].rotation);

        EnemiesInGame++;

        currentEnemiesInGame.Add(newEnemy);


    }

    void SaveHighScore()
    {
        PlayerPrefs.SetInt("hiscore", hiscore);
        HiScore.text = "High Score: " + PlayerPrefs.GetInt("hiscore").ToString();
    }

    void UpdateUI()
    {
        ChargeFill.fillAmount = player.shotCharge;

        currentScore.text = "Score: " + score.ToString();

        HealthFill.fillAmount = playerHealth;

        if (playerLives == 3)
        {
            lives[0].enabled = true;
            lives[1].enabled = true;
            lives[2].enabled = true;
        }

        if (playerLives == 2)
        {
            lives[0].enabled = false;
            lives[1].enabled = true;
            lives[2].enabled = true;
        }

        if (playerLives == 1)
        {
            lives[0].enabled = false;
            lives[1].enabled = false;
            lives[2].enabled = true;
        }

        if (playerLives <= 0)
        {
            lives[0].enabled = false;
            lives[1].enabled = false;
            lives[2].enabled = false;
        }


    }

    public void ResetGame()
    {
        Debug.Log("The Player Died.....Reseting");

        player.gameObject.SetActive(false);
        player.gameObject.transform.position = new Vector3(0f, -2f);

        playerHealth = 1f;
        playerLives--;

        if (playerLives == 0)
        {
            Debug.Log("YOU LOSE!!!");
        }

        for (int i = 0; i < AsteroidsInField.Count; i++)
        {
            Destroy(AsteroidsInField[i]);
            AsteroidsInField.Remove(AsteroidsInField[i]);
            AsteroidsInGame--;
        }


        player.gameObject.SetActive(true);
    }
}
