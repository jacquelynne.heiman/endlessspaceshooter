﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.ExceptionServices;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Rigidbody2D rb;
    float directionX;
    float moveSpeed = 10f;

    public float shotCharge = 0f;
    private float chargeTime = 3f;

    public float touchTime = 0;

    public GameObject shotPrefab;
    public GameObject chargedPrefab;
    public Transform shotSpawn;

    public GameManager gm;



    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        directionX = Input.acceleration.x * moveSpeed;
        transform.position = new Vector2(Mathf.Clamp(transform.position.x, -1.2f, 1.2f), Mathf.Clamp(transform.position.y, -2f, -2f));

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began && touchTime < .25f)
            {
                Instantiate(shotPrefab, shotSpawn.position, shotSpawn.rotation);
                touchTime = 0;
            }

            if (touch.phase == TouchPhase.Stationary && touchTime > .25f)
            {
                shotCharge += (1 / chargeTime) * Time.deltaTime;
                
            }

            if (shotCharge > 0 && touch.phase == TouchPhase.Ended)
            {
                Instantiate(chargedPrefab, shotSpawn.position, shotSpawn.rotation);
                shotCharge = 0;
                touchTime = 0;
            }

            touchTime += .5f * Time.deltaTime;

        }
    }

    void FixedUpdate()
    {
        rb.velocity = new Vector2(directionX, 0f);   
    }

    public void KillPlayer()
    {
        gm.ResetGame();
    }

}
