﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour
{
    //the speed of the background
    float speed = .5f;
    //the start position of the background
    Vector2 startPosition;

    void Start()
    {
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        float newPosition = Mathf.Repeat(Time.time * speed, 12f);
        transform.position = startPosition - Vector2.up * newPosition;
    }
}
